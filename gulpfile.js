const gulp = require('gulp')

// gulp pulgin
const minifyHTML = require('gulp-minify-html')
const HTMLReplace = require('gulp-html-replace')
const minifyCSS = require('gulp-minify-css')
const rename = require('gulp-rename')
const imagemin = require('gulp-imagemin')
const base64 = require('gulp-base64-inline')
// del pulgin
const del = require('del')

// basic path options
const path = {
  src: 'src',
  dest: 'dist'
}

// clean dist
const clean = () => del([path.dest])

// build css
const css = () => {
  return gulp
    .src(path.src + '/**/*.css')
    .pipe(minifyCSS())
    .pipe(base64('../images'))
    .pipe(
      rename(p => {
        p.basename += '.min'
        p.extname = '.css'
      })
    )
    .pipe(gulp.dest(path.dest))
}

// build html
const html = () => {
  const opts = { comments: false, spare: false, quotes: true }
  return gulp
    .src(path.src + '/*.html')
    .pipe(
      HTMLReplace({
        cssInline: {
          src: gulp
          .src(path.src + '/css/style.css')
          .pipe(minifyCSS())
          .pipe(base64('../images')),
          tpl: '<style>%s</style>'
        }
      })
    )
    .pipe(
      base64('./images', {
        prefix: '',
        suffix: ''
      })
    )
    .pipe(minifyHTML(opts))
    .pipe(gulp.dest(path.dest))
}

// build image
// const image = () => {
//   const opts = { interlaced: true, progressive: true, optimizationLevel: 5 }
//   return (
//     gulp
//       .src(path.src + '/images/**/*')
//       // .pipe(imagemin())
//       .pipe(gulp.dest(path.dest + '/images/'))
//   )
// }

// command : gulp  清除dist檔案 壓縮css html 替換路徑
gulp.task('default', gulp.series(clean, html))
